export default interface Users {
  id: number;
  login: string;
  name: string;
  password: string;
}
